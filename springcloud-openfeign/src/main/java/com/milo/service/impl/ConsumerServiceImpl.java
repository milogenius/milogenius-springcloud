package com.milo.service.impl;

import com.milo.client.ConsumerClient;
import com.milo.service.IConsumerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: Milogenius
 * @create: 2019-06-28 13:49
 * @description:
 **/
@Service
public class ConsumerServiceImpl implements IConsumerService {

    @Resource
    private ConsumerClient client;

    @Override
    public String hello(String name) {
        return client.hello();
    }
}
