package com.milo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * OpenFeign
 * @author milogenius
 * @date 2020/3/16 21:11
 * @version v1.0.0
 * @description
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OpenFeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenFeignApplication.class, args);
	}

}
