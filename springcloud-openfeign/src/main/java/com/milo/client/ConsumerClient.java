package com.milo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author milogenius
 * @date 2020/3/160:03
 * @description
 */
@FeignClient("service-provider")
@Component
public interface ConsumerClient {

       @GetMapping("/hello")
       String hello();
}
