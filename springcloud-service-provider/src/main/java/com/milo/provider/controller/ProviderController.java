package com.milo.provider.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Milogenius
 * @create: 2019-06-28 12:14
 * @description:
 **/
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String port;


    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String index(){
        log.info("hello");
        return "hello world from Port:"+port;
    }
}
