package com.milo.fallback;

import com.milo.client.ProviderClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 针对服务提供者Client做降级处理
 * @author milogenius
 * @date 2020/3/22 23:22
 * @version v1.0.0
 * @description
 */
@Component
@Slf4j
public class ProviderClientFallBack implements ProviderClient {

       @Override
       public String hello() {
              log.info("feign 调用服务提供者服务异常");
              return null;
       }
}
