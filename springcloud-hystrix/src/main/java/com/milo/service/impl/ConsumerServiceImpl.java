package com.milo.service.impl;

import com.milo.client.ProviderClient;
import com.milo.service.IConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: Milogenius
 * @create: 2019-06-28 13:49
 * @description:
 **/
@Service
public class ConsumerServiceImpl implements IConsumerService {

    @Autowired
    private ProviderClient client;
    @Override
    public String hello(String name) {
        return client.hello();
    }
}
