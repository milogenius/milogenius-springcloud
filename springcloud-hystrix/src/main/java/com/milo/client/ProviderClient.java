package com.milo.client;

import com.milo.fallback.ProviderClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author milogenius
 * @date 2020/3/160:03
 * @description
 */
@FeignClient(value = "service-provider",fallback = ProviderClientFallBack.class)
@Component
public interface ProviderClient {

       @GetMapping("/hello")
       String hello();
}
