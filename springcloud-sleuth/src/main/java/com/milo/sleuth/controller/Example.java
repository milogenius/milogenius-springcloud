package com.milo.sleuth.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class Example {



    @RequestMapping("/")
    String home() {
        log.info("Hello world!");
        return "Hello World!";
    }


}