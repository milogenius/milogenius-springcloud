package com.milo.sentinel.service;

/**
 * @author Milo Lee
 * @date 2021-03-23 11:34
 */
public interface ISentinelService {

    String hello (long s);
}
