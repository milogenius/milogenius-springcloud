package com.milo.sentinel.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.milo.sentinel.service.ISentinelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Milo Lee
 * @date 2021-03-23 11:34
 */
@Service
@Slf4j
public class SentinelServiceImpl implements ISentinelService {


    /**
     *Sentinel 提供了 @SentinelResource 注解用于定义资源
     * @param s
     * @return
     */
    @Override
    //value：资源名称，必需项（不能为空）
    //blockHandler 对应处理 BlockException 的函数名称
    //fallback  用于在抛出异常的时候提供 fallback 处理逻辑
    @SentinelResource(value = "hello", blockHandler = "exceptionHandler", fallback = "helloFallback")
    public String hello(long s) {
        log.error("hello:{}",s);
        return String.format("Hello at %d", s);
    }

    // Fallback 函数，函数签名与原函数一致或加一个 Throwable 类型的参数.
    public String helloFallback(long s) {
        log.error("helloFallback:{}",s);
        return String.format("Halooooo %d", s);
    }

    // Block 异常处理函数，参数最后多一个 BlockException，其余与原函数一致.
    public String exceptionHandler(long s, BlockException ex) {
        // Do some log here.
        log.error("exceptionHandler:{}",s);
        ex.printStackTrace();
        return "Oops, error occurred at " + s;
    }



    // 这里单独演示 blockHandlerClass 的配置.
    // 对应的 `handleException` 函数需要位于 `ExceptionUtil` 类中，并且必须为 public static 函数.
 /*   @SentinelResource(value = "test", blockHandler = "handleException", blockHandlerClass = {ExceptionUtil.class})
    public void test() {
        System.out.println("Test");
    }*/
}
