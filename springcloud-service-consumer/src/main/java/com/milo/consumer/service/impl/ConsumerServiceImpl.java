package com.milo.consumer.service.impl;

import com.milo.consumer.service.IConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author: Milogenius
 * @create: 2019-06-28 13:49
 * @description:
 **/
@Service
public class ConsumerServiceImpl implements IConsumerService {

    @Autowired
    RestTemplate restTemplate;
    @Override
    public String hello(String name) {
        return restTemplate.getForObject("http://SERVICE-PROVIDER:8762/hello?name="+name,String.class);
    }
}
