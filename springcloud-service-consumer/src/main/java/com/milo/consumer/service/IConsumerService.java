package com.milo.consumer.service;

/**
 * @author: Milogenius
 * @create: 2019-06-28 13:48
 * @description:
 **/
public interface IConsumerService {

    String hello(String name);
}
