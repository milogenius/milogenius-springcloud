package com.milo.consumer.controller;

import com.milo.consumer.service.IConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Milogenius
 * @create: 2019-06-28 13:45
 * @description:
 **/
@RestController
public class ConsumerController {

    @Autowired
    private IConsumerService consumerService;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String index(@RequestParam String name){
        return  consumerService.hello(name);
    }
}
